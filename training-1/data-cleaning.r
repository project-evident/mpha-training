library(readxl)
library(readr)
library(dplyr)
library(lubridate)
library(ggplot2)


raw_data = read_excel("data/All Data.xlsx", sheet = "ClosedWO")

data = raw_data %>% 
  # change Date columns to Date class
  mutate_at(vars(contains("Date")), as.Date) %>%
  mutate(success_yn = ifelse(Success == 1, "Yes", "No"),
         # reorder the Urgency factor by the Goal
         Urgency = reorder(factor(Urgency), Goal)
)


# table for board report
data %>%
  filter(OpenDate > ymd('2018-02-01'), OpenDate < ymd("2019-02-01"))  %>%
  group_by(Urgency) %>%
  summarize(`Total Completed` = n(),
            `% Meeting Goal` = scales::percent(mean(Success)),
            `Avg Days Open` = round(mean(`Days Open`)),
            `90th Percentile` = quantile(`Days Open`, 0.9))
# We could save this as a CSV by tacking on %>% write_csv("board_table.csv")
# Next time we'll cover putting this into a document



## Little bit of plotting:  ####
plot_data = data %>% filter(OpenDate > ymd('2018-02-01'), OpenDate < ymd("2019-02-01"))

## start with a bar plot
ggplot(plot_data, aes(x = floor_date(OpenDate, "month"), fill = success_yn)) +
  geom_bar(position = position_dodge()) +
  scale_x_date(date_labels = "%b") +
  labs(x = "Open date", y = "Number of Work Orders")

## area plot, like in the power point
area_plot = ggplot(plot_data, aes(x = floor_date(OpenDate, "month"), fill = success_yn)) +
  stat_count(geom = "area") +
  scale_x_date(date_labels = "%b")

area_plot

## Let's make it pretty!
## Building this up iteratively so you can see what each line does
## We'll put it together at the end
library(ggthemes)
area_plot + theme_tufte()

# Theme functions let you change the base font size and family
# It's called the "base" font size because some text is sized
# relative to the base, e.g., the tick labels are 0.8 * base_size
# (or something like that)
area_plot + theme_tufte(base_size = 20, base_family = "sans")

area_plot + theme_tufte(base_size = 20, base_family = "sans") +
  labs(x = "Open date", y = "Number of Work Orders", fill = "Met goal")

area_plot + theme_tufte(base_size = 20, base_family = "sans") +
  labs(x = "Open date", y = "Number of Work Orders", fill = "Met goal") +
  scale_fill_colorblind()

area_plot + theme_tufte(base_size = 20, base_family = "sans") +
  labs(x = "Open date", y = "Number of Work Orders", fill = "Met goal") +
  scale_fill_colorblind() +
  scale_y_continuous(labels = scales::comma) 

# we get a warning here because the original `area_plot` already had 
# an X scale, and we add a new one.
# Warnings are not errors. Errors stop your code and must be fixed.
# Warnings let you know that your code ran, but something *might* be wrong.
# You use your judgment whether or not it needs fixing. Here, I appreciate
# the warning, but I'm deliberately replacing the old X-scale, so I don't
# worry about it.
area_plot + theme_tufte(base_size = 20, base_family = "sans") +
  labs(x = "Open date", y = "Number of Work Orders", fill = "Met goal") +
  scale_fill_colorblind() +
  scale_y_continuous(labels = scales::comma) +
  scale_x_date(date_labels = "%b '%y", 
               date_breaks = "1 month",
               expand = c(0, 0)) 

# Update the plot object, storing all these changes in `area_plot`
area_plot =  area_plot +
  theme_tufte(base_size = 12, base_family = "sans") + 
  labs(x = "Open date", y = "Number of Work Orders", fill = "Met goal") +
  scale_fill_colorblind() +
  scale_y_continuous(labels = scales::comma) +
  scale_x_date(date_labels = "%b '%y", 
               date_breaks = "1 month",
               expand = c(0, 0)) 

# Saving it to disk
ggsave(
  area_plot,
  filename = "work-orders.png",
  width = 8,
  height = 4,
  dpi = 180
)

## Experiment with some other plots

ggplot(plot_data, aes(
  x = reorder(`Type of Work Order`, Success),
  fill = success_yn
)) +
  geom_bar() +
  facet_wrap( ~ Urgency, scales = "free") +
  coord_flip() +
  theme_minimal()

# Same plot with a fancier weighting function for the ordering.
# I add 1 to the numerator and 2 to the denominator when calculating
# the mean. This weights the means a little bit towards 1/2, so that 
# Types with few counts won't be quite so extreme.
ggplot(plot_data, aes(
  x = reorder(`Type of Work Order`, Success, function(x)
    (sum(x) + 1) / (length(x) + 2)),
  fill = success_yn
)) +
  geom_bar() +
  facet_wrap( ~ Urgency, scales = "free") +
  coord_flip() +
  theme_minimal()

