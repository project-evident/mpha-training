## Connecting to SQL

library(RODBC)
# or
library(odbc)
# I looked into `odbc`, and I haven't found any reason RODBC is better.
# And some say odbc is faster.
# I'll keep the code examples as RODBC since that's what I'm familiar with
# and I can't test :)
# But you should be able to use obdc::dbConnect instead of ROBDC::odbcConnect
# and odbc::dbSendQuery instead of RODBC::sqlQuery


elite = odbcConnect(
  # need these at least
  dsn = "EliteLive", uid = "readonly", pwd = "p@ssword",
  # might want some extras
  readOnlyOptimize = TRUE 
)

vh = odbcConnect(
  # need these at least
  dsn = "VisualHOMES", uid = "readonly", pwd = "p@ssword",
  # might want some extras
  readOnlyOptimize = TRUE 
)


# You can configure DSN nicknames in Windows Control Panel/Administrative Tools/Data Sources (ODBC)

## Secrets ####

# But what about the password??
# Don't want to put in in your R file

# one option:
vh = odbcConnect(
  # need these at least
  dsn = "VisualHOMES", uid = "readonly",
  pwd = rstudioapi::askForPassword("VisualHOMES password"),
  # might want some extras
  readOnlyOptimize = TRUE 
)
## This will prompt for password every time with a disguised prompt
## It is safe if used immediately, but don't store it
my_password = rstudioapi::askForPassword("don't store this")
print(my_password)
# may be save in R History


## Nicer way: save it in a user-specific R environment file
file.edit("~/.Renviron")
## add a line like this
## elite_pw = "p@ssword"
## will be run when R starts. Access like this:
Sys.getenv("elite_pw")
## again, don't save this in your current R environment, just use it
vh = odbcConnect(
  # need these at least
  dsn = "VisualHOMES", uid = "readonly", pwd = Sys.getenv("elite_pw"),
  # might want some extras
  readOnlyOptimize = TRUE 
)

## only downside above is that the password is still stored in plain text on your computer
## if that worries you, use the keyring package. The mechanics are similar, but it
## works with Windows's secret manager
keyring::key_set("elite_pw") # set it once
keyring::key_get("elite_pw") # get and use immediately
## This is a really nice way to do it!

## Most of this info is from this document:
# https://cran.r-project.org/web/packages/httr/vignettes/secrets.html


## Querying ####


# if you ever need whole table
sqlFetch(elite, "workorders")
# or top n rows
sqlFetch(elite, "workorders", max = 500)

# you can check table names
sqlTables(elite, tableName = "housing*")

# but mostly we use sqlQuery 
result = sqlQuery(con = elite, "select top 10 * from mytable where mycol = 2")

# Short queries are well and good inside your R files. Longer queries, you probably
# want to keep in separate .sql files.

library(readr)
wo_query = read_file("sample-query.sql")
# sometimes (depending on drivers and database systems) this
# won't work right if there are commentsin the SQL file with two dashes --
# Let me know if this is true for you, and I'll help work around it.
# Try this sample query (correct the table name first!) to see if it works.

# often nicer
sqlQuery(elite, query = wo_query, stringsAsFactors = FALSE)

# when you're done, some people say close the connection
# (but this doesn't seem to really matter... if things are working
#  try to close and reopen. Apparrently it's important for a MS Access database)
odbcClose(elite)
odbcClose(vh)
# or
odbcCloseAll()

# Lots more details here
# RODBC: https://cran.r-project.org/web/packages/RODBC/vignettes/RODBC.pdf
# odbc: https://db.rstudio.com/odbc/

