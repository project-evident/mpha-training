# mpha-training

Materials for R training with MPHA

## Session 1 - 2019-03-05

Handling data

- Project organization
- Import/Export with files
- Connecting to SQL
- Data Manipulation
- Plotting demo


## Session 2 - TBD

Automated reporting

- How knitr works
- Markdown basics
- Code chunks and options
- Formatting tables

Other topics may include:

- ggplot2
- Demos of
    - mapping
    - modeling
    - other?